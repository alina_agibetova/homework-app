import { Component, OnDestroy, OnInit } from '@angular/core';
import { Message } from '../shared/message.model';
import { Subscription } from 'rxjs';
import { MessageService } from '../shared/message.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit, OnDestroy {
  messages: Message[]= [];
  postsChangeSubscription!: Subscription;
  postFetchingSubscription!: Subscription;
  isFetching = false;

  constructor(private messageService: MessageService) { }

  ngOnInit(): void {
    this.postsChangeSubscription = this.messageService.postsChange.subscribe((messages: Message[]) => {
      this.messages = messages.reverse();
    });

    this.postFetchingSubscription = this.messageService.postsFetching.subscribe((isFetching: boolean)=> {
      this.isFetching = isFetching;
    });
    this.messageService.fetchPosts();
  }

  ngOnDestroy(): void {
    this.postsChangeSubscription.unsubscribe();
    this.postFetchingSubscription.unsubscribe();
  }

}
