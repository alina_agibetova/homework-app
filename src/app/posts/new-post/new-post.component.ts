import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { MessageService } from '../../shared/message.service';
import { Message } from '../../shared/message.model';


@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.css']
})
export class NewPostComponent implements OnInit, OnDestroy {
  @ViewChild('message')postForm!: NgForm;
  isUpLoading = false;
  postUpLoadingSubscription!: Subscription;
  createId = '';

  constructor(private messageService: MessageService) { }

  ngOnInit(): void {
    this.postUpLoadingSubscription = this.messageService.postUpLoading.subscribe((isUpLoading: boolean) => {
      this.isUpLoading = isUpLoading;
    });
  }

  savePost(){
    const id = this.createId || Math.random().toString();
    const post = new Message(id, this.postForm.value.author, this.postForm.value.message, this.postForm.value.datetime);
    const next = () => {
      this.messageService.fetchPosts();
    };
    this.messageService.addPost(post).subscribe(next);
  }

  ngOnDestroy(): void {
    this.postUpLoadingSubscription.unsubscribe();
  }

}
