import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Message } from './message.model';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';

@Injectable()
export class MessageService {
  postsChange = new Subject<Message[]>();
  postsFetching = new Subject<boolean>();
  postUpLoading = new Subject<boolean>();
  private messages: Message[] = [];


  constructor(private http: HttpClient){}

  fetchPosts(){
    this.postsFetching.next(true);
    this.http.get<{[id: string]: Message}>('http://146.185.154.90:8000/messages')
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const data = result[id];
          return new Message(id, data.author, data.message, data.datetime);
        })
      })).subscribe(messages => {
        this.messages = messages;
        this.postsChange.next(this.messages.slice());
        this.postsFetching.next(false);
    }, error => {
        this.postsFetching.next(false);
    })
  }

  getPosts(){
    return this.messages.slice();
  }

  addPost(message: Message){
    const body = new HttpParams()
      .set('author', message.author)
      .set('message', message.message);

      this.postUpLoading.next(true);

      return this.http.post('http://146.185.154.90:8000/messages', body).pipe(
        tap(()=> {
          this.postUpLoading.next(false);
        }, ()=> {
          this.postUpLoading.next(false);
        })
      )

  }


}
